package com.island;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.island.mapper.BlogMapper;
import com.island.model.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class WrapperTest {

    @Autowired
    private BlogMapper blogMapper;

    @Test
    void contextLoads(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNotNull("title")
                    .isNotNull("author")
                    .ge("id",1L);
        blogMapper.selectList(queryWrapper).forEach(System.out::println);

    }

    @Test
    public void test02(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("views","9998");
        System.out.println(blogMapper.selectOne(queryWrapper));
    }

    @Test
    public void test03(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.between("views",100,9998);
        System.out.println(blogMapper.selectCount(queryWrapper));
    }

    @Test
    public void test04(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.notLike("title","s").likeRight("title","j");
        blogMapper.selectMaps(queryWrapper).forEach(System.out::println);

    }

    @Test
    public void test05(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.inSql("id","select id from blog where id<3");
        List<Object> objects = blogMapper.selectObjs(queryWrapper);
        objects.forEach(System.out::println);

    }

    @Test
    public void test06(){
        QueryWrapper<Blog> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        List<Blog> objects = blogMapper.selectList(queryWrapper);
        objects.forEach(System.out::println);

    }
}
