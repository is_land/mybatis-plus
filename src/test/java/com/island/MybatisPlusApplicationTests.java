package com.island;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.island.mapper.BlogMapper;
import com.island.model.Blog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@SpringBootTest
class MybatisPlusApplicationTests {

    @Autowired
    private BlogMapper blogMapper;

    @Test
    void contextLoads() {
        List<Blog> blogs=blogMapper.selectList(null);
        blogs.forEach(System.out::println);
    }

    @Test
    public void testInsert(){
        Blog blog = new Blog();
        //blog.setId("5");
        blog.setTitle("bbbbbb");
        blog.setAuthor("island");
        blog.setViews(123);
        int res = blogMapper.insert(blog);
        System.out.println(res);
        System.out.println(blog);
    }

    @Test
    public void testUpdate(){
        Blog blog = new Blog();
        blog.setId(5L);
        blog.setTitle("aaaaaa");
        blog.setAuthor("island");
        blog.setViews(121);
        int res = blogMapper.updateById(blog);
        System.out.println(res);
        System.out.println(blog);
    }

    // 乐观锁成功情况！
    @Test
    public void testOptimisticLocker1(){
        Blog blog = blogMapper.selectById(5L);

        blog.setTitle("aaaa");
        blog.setViews(126);
        blogMapper.updateById(blog);
    }

    //乐观锁失败情况！
    @Test
    public void testOptimisticLocker2(){
        //线程1
        Blog blog = blogMapper.selectById(5L);

        blog.setTitle("aaaa");
        blog.setViews(126);
        Blog blog1 = blogMapper.selectById(5L);
        blog1.setTitle("aaa");
        blog1.setViews(122);
        blogMapper.updateById(blog1);
        blogMapper.updateById(blog);
    }

    @Test
    public void testSelectById(){
        List<Blog> blogs = blogMapper.selectBatchIds(Arrays.asList(5L));
        blogs.forEach(System.out::println);
    }

    @Test
    public void testSelectByMap(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("author","狂神说");
        List<Blog> blogs = blogMapper.selectByMap(map);
        blogs.forEach(System.out::println);
    }

    @Test
    public void testPage(){
        Page<Blog> blogPage = new Page<Blog>(2,5);
        blogMapper.selectPage(blogPage,null);
        blogPage.getRecords().forEach(System.out::println);
        System.out.println(blogPage.getTotal());
    }

    @Test
    public void testDel(){
        int res = blogMapper.deleteById(5L);
    }

}
