package com.island.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.island.model.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogMapper extends BaseMapper<Blog> {
}
