package com.island.blog.service;

import com.island.blog.model.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author island
 * @since 2021-04-29
 */
public interface BlogService extends IService<Blog> {

}
