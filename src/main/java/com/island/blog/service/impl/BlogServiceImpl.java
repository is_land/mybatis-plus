package com.island.blog.service.impl;

import com.island.blog.model.Blog;
import com.island.blog.mapper.BlogMapper;
import com.island.blog.service.BlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author island
 * @since 2021-04-29
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements BlogService {

}
