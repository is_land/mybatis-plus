package com.island.blog.mapper;

import com.island.blog.model.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author island
 * @since 2021-04-29
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
