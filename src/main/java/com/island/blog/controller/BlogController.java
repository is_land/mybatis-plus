package com.island.blog.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author island
 * @since 2021-04-29
 */
@RestController
@RequestMapping("/blog/blog")
public class BlogController {

}

