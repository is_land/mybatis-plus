package com.island.model;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Blog {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String title;
    private String author;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    private int views;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
    @Version
    private int version;

    @TableLogic
    private int deleted;
}
